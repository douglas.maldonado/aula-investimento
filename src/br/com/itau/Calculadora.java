package br.com.itau;

public class Calculadora {
    private double valor;
    private int meses;

    public Calculadora(double valor, int meses) {
        this.valor = valor;
        this.meses = meses;
    }

    public double getRendimento(){
        double rendimento = valor;

        for (int i = 0; i < meses; i++){
            rendimento += rendimento * 0.007;
        }
        return rendimento;
    }


}
