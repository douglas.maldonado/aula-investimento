package br.com.itau;

public class Banco {
    private double valorInvestido;
    private int meses;

    public Banco(double valorInvestido, int meses) {
        this.valorInvestido = valorInvestido;
        this.meses = meses;
    }

    public Banco(){}

    public double getValorInvestido() {
        return valorInvestido;
    }

    public int getMeses() {
        return meses;
    }

    public void setValorInvestido(double valorInvestido) {
        this.valorInvestido = valorInvestido;
    }

    public void setMeses(int meses) {
        this.meses = meses;
    }
}
