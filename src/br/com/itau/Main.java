package br.com.itau;

import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        //criar uma Classe Scanner
        Scanner scanner = new Scanner(System.in);
        Banco banco = new Banco();

        System.out.println("Digite Valor do Investimento: ");
        banco.setValorInvestido(scanner.nextDouble());

        System.out.println("Digite Quantidade de Meses de Rendimento: ");
        banco.setMeses(scanner.nextInt());

        //chamar a Calculadora dentro da classe Banco
        Calculadora calculadora = new Calculadora(banco.getValorInvestido(), banco.getMeses());

        //criar uma Classe impressora
        System.out.println("O valor do retorno será: " + calculadora.getRendimento());

    }


}
